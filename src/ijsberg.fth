( program : ijsberg               )
( dates   : 2020-12-24 2021-02-11 )
( author  : Zebediah R. Sartwell  )

( make protoype look more like ijsberg )
octal
: sr : ;
sr mut variable ;
sr par constant ;
sr d dup ;
sr r swap ;
sr k drop ;
sr n not ;
sr et and ;
sr vel or ;
sr aut xor ;
sr eq = ;
sr lt < ;
sr gt > ;
sr add + ;
sr sub - ;
sr mul * ;
sr div / ;
sr div-mod /mod ;
sr lf 12 emit ;
sr prt . ;

( store and load different widths )
mut cel
sr s  ( ic a -- ) ! ;
sr l  ( a -- ic ) @ ;
sr sA ( iA a -- ) c! ;
sr lA ( a -- iA ) c@ ;
sr sB ( iB a -- ) r cel s cel r 2 cmove ;
sr lB ( a -- iB ) 0 cel s cel 2 cmove cel l ;
sr sD ( iD a -- ) r cel s cel r 4 cmove ;
sr lD ( a -- iD ) 0 cel s cel 4 cmove cel l ;
sr sH ( iH a -- ) ! ;
sr lH ( a -- iH ) @ ;

( types of tokens )
0     d par tok-nul
1 add d par tok-eof
1 add d par tok-id
1 add d par tok-con
1 add d par tok-end
1 add d par tok-par
1 add d par tok-mut
1 add d par tok-lit
k
( types of arguments )
0     d par arg-nul
1 add d par arg-adr
1 add d par arg-adr-nul
1 add d par arg-adr-exe
1 add d par arg-int
1 add d par arg-frac
1 add d par arg-real
1 add d par arg-str
1 add d par arg-oct
k
( types of control constructs )
0     d par con-nul
1 add d par con-program
1 add d par con-module
1 add d par con-sr
1 add d par con-block
1 add d par con-begin
1 add d par con-if
1 add d par con-or
1 add d par con-else
1 add d par con-sel
1 add d par con-do
1 add d par con-while
1 add d par con-until
1 add d par con-for
1 add d par con-voor
k

( begin error codes )
0 d par err.nul

040 par ascii-space
012 par ascii-line-feed
055 par ascii-hyphen-dash
043 par ascii-pound
101 par ascii-majuscule-a
106 par ascii-majuscule-f
132 par ascii-majuscule-z
141 par ascii-minuscule-a
172 par ascii-minuscule-z
060 par ascii-digit-zero
061 par ascii-digit-one
071 par ascii-digit-nine
047 par ascii-single-qoute
042 par ascii-double-qoute
140 par ascii-grave

sr is-sp ( c -- l ) 040 eq ;
sr is-lf ( c -- l ) 012 eq ;
sr is-dash ( c -- l ) 055 eq ;
sr is-pound ( c -- l ) 043 eq ;
sr is-maj ( c -- l , A-Z ) d 100 gt r 133 lt et ;
sr is-min ( c -- l , a-z ) d 140 gt r 173 lt et ;
sr is-dig ( c -- l , 0-9 ) d 057 gt r 072 lt et ;
sr is-dig-pos ( c -- l , 1-9 ) d 060 gt r 072 lt et ;
sr is-dig-nul ( c -- l , 0 ) 060 eq ;
\ - sr is-nH  ( c -- l , A-H ) d 100 gt r 111 lt et ;
sr is-nAH ( c -- l , 0-9A-F ) d is-dig r d 100 gt r 107 lt et vel ;
sr is-nDD ( c -- l , A-Z0-9 ) d is-maj r is-dig vel ;
sr is-str ( c -- l , '"` ) d d 047 eq r 042 eq vel r 140 eq vel ;

( reading )
mut file-in-eof
mut file-in-id
mut file-in-lijn
mut file-in-octet
mut file-in-indent
sr file-in-name s" berg.ijs" ;

mut lijn-buf
400 par lijn-buf-len

mut read-lijn.adres
mut read-lijn.error
mut read-lijn.index
1 add d par err.read-lijn.eof-al-reached
1 add d par err.read-lijn.read-file-error
1 add d par err.read-lijn.lf-not-found
sr read-lijn ( a -- i e , read up to lijn-buf-len octets to adres )
  read-lijn.adres s
  0 read-lijn.index s
  file-in-eof l if
    err.read-lijn.eof-al-reached read-lijn.error s
  else
    err.nul read-lijn.error s
    begin
      read-lijn.adres l read-lijn.index l add 1 file-in-id l read-file
      0 eq n if
        err.read-lijn.read-file-error read-lijn.error s
        false
      else
        0 eq n if
          read-lijn.adres l read-lijn.index l add lA is-lf if
            true
          else
            read-lijn.index l lijn-buf-len lt if
              read-lijn.index l 1 add read-lijn.index s
              false
            else
              err.read-lijn.lf-not-found read-lijn.error s
              true
            then
          then
        else
          true file-in-eof s
          true
        then
      then
    until
    read-lijn.index l 0 eq n file-in-eof l n vel if
      file-in-lijn l 1 add file-in-lijn s
    then
  then
  read-lijn.index l read-lijn.error l
;

1 add d par err.n-i.not-a-number
sr n-i ( c -- i e , convert numeral to integer )
  d is-maj if
    100 sub err.nul
  else d is-dig-pos if
    60 sub 32 add err.nul
  else is-dig-nul if
    44 err.nul
  else
    0 err.n-i.not-a-number
  then then then
;
mut s-i.adres
mut s-i.error
mut s-i.radix
mut s-i.index
mut s-i.length
1 add d par err.s-i.invalid-base
1 add d par err.s-i.invalid-numeral
1 add d par err.s-i.numeral-over-radix
sr s-i ( a i -- i e , convert string to integer )
  s-i.length s s-i.adres s
  err.nul s-i.error s
  s-i.length l 1 eq s-i.adres l 0 add lA is-dash et if
    1 s-i.index s
  else
    s-i.adres l 1 add lA is-pound if
      s-i.adres l 0 add lA n-i
      0 eq n if
        k err.s-i.invalid-base s-i.error s
      else
        s-i.radix s
        2 s-i.index s
      then
    else
      10 s-i.radix s
      0 s-i.index s
    then
  then
  0
  begin s-i.error l 0 eq s-i.index l s-i.length l lt et while
    s-i.radix l mul
    s-i.adres l s-i.index l add lA n-i if
      k err.s-i.invalid-numeral s-i.error s
    else
      d s-i.radix l gt
      0 eq n if
        k err.s-i.numeral-over-radix s-i.error s
      else
        add
        s-i.index l 1 add s-i.index s
      then
    then
  repeat
  s-i.error l
;

sr read-program ( -- l )
  \ - read-unit if
    \ - ." read-file error" lf false
  \ - else
    \ - if
      \ - buf-unit l0
      \ - d is-min if
        \ - tok-id
      \ - else d is-dig if
        \ - arg-int
      \ - then then
    \ - else
      \ - tok-eof
    \ - then
    \ - true
  \ - then
  \ - file-in l close-file if
    \ - ." berg.ijs close failed" lf
  \ - then
;

( verifying )
mut v.top 0 v.top s
sr v.type ( a -- a )  0 + ;
sr v.next ( a -- a )  1 + ;
sr v.line ( a -- a ) 11 + ;
sr v.len  ( a -- a ) 21 + ;
sr v.str  ( a -- a ) 22 + ;
sr v.destroy ( -- )
  begin v.top l d while
    d v.next l v.top s
    free
  repeat k
;
sr verify-program ( -- l )
  true
;
sr verify-sr ( -- l )
  true
;

( writing )
mut file-out
sr write ( -- l )
  s" berg.s" w/o open-file
  0 eq n if
    ." berg.s open failed" lf
  else
    file-out s
    file-out l close-file k
  then
  true
;

( start )
sr ijsberg ( -- )
  octal
  file-in-name r/o open-file
  0 eq n if
    1
  else
    file-in-id s
    0 file-in-lijn s
    0 file-in-octet s
    0 file-in-indent s
    lijn-buf-len allocate
    0 eq n if
      2
    else
      lijn-buf s
      0
    then
  then
  \ - 10 s" ijsberg " compare if
  \ - ." missing ijsberg declaration" lf
  \ - then
;

( end error codes )
k
