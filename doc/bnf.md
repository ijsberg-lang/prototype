```
; ijsberg syntax. 2020/07/26-2021/02/11.

file = program | module .

; Subprograms.
program_unit = program | module .
program
  = "ijsberg" version lf
    "program" lf
      ((statement | sr_call) (lf | ";"))* lf
    "contains" lf
      (sr_def lf)*
    "end program" .
module
  = "ijsberg" version lf
    "module" lf
      ((statement | sr_call) (lf | ";"))* lf
    "contains" lf
      (sr_def lf)*
    "end module" .
version = dozenal+ "," dozenal+ "," dozenal .

; Procedures.
sr_dec = "sr" sr_id  .
sr_def
  = "sr" sr_id dummy_argument* lf
      (statement | sr_call)*
    "contains"
    "." b "sr" b sr_id .
sr_call = sr_id argument* .
sr_id = identifier .

; Control constructs.
control_construct = do | if | sel .
do
  = name? b "do" b "infinite"?
      (((statement | sr_call) sep)* lf)*
    "." b "do" b name? .
if
  = name? b "if" b condition lf
      statement*
    ("or" b condition lf
      statement)*
    ("else" lf
      statement*)?
    "." b "if" b name? .
sel = "sel" argument+ lf .
begin = "begin" lf statement* .
cycle = "cycle" b name (b condition)? .
exit = "exit" b name (b condition)? .
for = "for" b numeric_literal b numeric_literal (b numeric_literal)? .

; Statements.
statement
  = sp* (control_construct
  | ((state | subroutine_call) (lf | ";")+) lf) .
continue = "continue" .

; Variables.
variable_declaration
  = sp* (parameter_declaration | mutable_declaration
  | dummy_argument_declaration | global_access) .
parameter_declaration
  = "par" b identifier "=" literal .
mutable_declaration
  = "mut" b identifier ("=" literal)? .
dummy_argument_declaration = .
global_access = intent b identifier (b identifier)+ .
variable_attribute = type kind .
type = "l" | "i" | "r" | "f" | "s" .
kind = base6+ .
intent = "in" | "out" | "inout" | "val" .

; Expressions.
expressions = .
condition = .

; Literals.
literal = logical_literal | numeric_literal | string_literal .
logical_literal = "true" | "false" .
numeric_literal = integer_literal | real_literal | fraction_literal .
integer_literal = base8+ ("r" base36+ | "j" base36*)? negative? .
real_literal
  = base8+  (("+" | "-") base8+
  | "r" base36+ ("+" | "-") base36+
  | "j" base36* ("+" | "-") base36*) negative? .
fraction_literal
  = base8+ ((rad_kar base8+)? fra_kar base8+
  | "r" base36+ (rad_kar base36+)? fra_kar base36+
  | "j" base36* (rad_kar base36*)? fra_kar base36+) negative? .
base8  = <0x30..0x37> .
base10 = <0x30..0x39> .
base16 = <0x41..0x46> | base10 .
base36 = majuscule | base10 .
rad_kar = "," .
fra_kar = "/" .
negative = "-" .
string_literal
  = ("'" | '"')
  (<0x00..0x09> | <0x0B..0x21> | <0x24..0xFF> | "''" | "'#")* "'"
  | "@" (base8 base8 base8 "."?)+
  | "$" (base16 base16 "."?)+ .

; Operators.
not = "`"
and = "&" . nand = "&/" .
or  = "|" . nor  = "|/" . xor = "||" .
eq  = "=" . neq  = "=/" .
lt  = "<" . nlt  = "</" .
gt  = ">" . ngt  = ">/" .
math_op = "+" | "-" | "*" | "/" | "**" | "%" | "%%" .
concat = "//" .

; Miscellaneous.
b = sp+ ("#" sp* lf (comment? | lf | sp)* "#")? . ; Blanks
comment = "!" (<0x00..0x09> | <0x0B..0xFF>)* lf .
name = identifier .
identifier = minuscule (minuscule | ".")* .
letter = majuscule | minuscule .
majuscule = <0x41..0x5A> . ; [A-Z]
minuscule = <0x61..0x7A> . ; [a-z]
lf = <0x0A> .
sp = <0x20> .
```
