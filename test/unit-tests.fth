( program : ijsberg )
( dates   : 2021-02-10 2021-02-10 )
( author  : Zebediah R. Sartwell )
octal

( k lijn-buf l read-lijn lijn-buf l lA emit )

lf ." testing n-i" lf
." A : " s" A" k lA n-i .s k k
." B : " s" B" k lA n-i .s k k
." C : " s" C" k lA n-i .s k k
." 1 : " s" 1" k lA n-i .s k k
." 2 : " s" 2" k lA n-i .s k k
." 3 : " s" 3" k lA n-i .s k k
." 0 : " s" 0" k lA n-i .s k k
." - : " s" -" k lA n-i .s k k

lf ." testing s-i" lf
." AD : " s" AD" s-i .s k k
." DD : " s" DD" s-i .s k k
." GH : " s" GH" s-i .s k k
." CGH : " s" CGH" s-i .s k k
." H#AD : " s" H#AD" s-i .s k k
." A#AAA : " s" A#AAA" s-i .s k k
." 0#0 : " s" 0#0" s-i .s k k
." 0# : " s" 0#" s-i .s k k
." -#AD : " s" -#AD" s-i .s k k
." H#- : " s" H#-" s-i .s k k
." - : " s" -" s-i .s k k
." -- : " s" --" s-i .s k k
." HI : " s" HI" s-i .s k k
." H#HI : " s" H#HI" s-i .s k k
